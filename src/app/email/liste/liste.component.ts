import { Component, OnInit } from '@angular/core';
import { FAKE_EMAILS_DATA } from '../../data';
import { Email } from '../types';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, filter, map } from 'rxjs';

@Component({
  selector: 'app-emails-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
  emailsAndTitle$?: Observable<{ emails: Email[], title: string }>;
  type?: string | null;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  goToEmail(id: number) {
    if (!this.type) {
      this.router.navigate(['./', 'read', id],
        { relativeTo: this.route }
      );
      return;
    }
    this.router.navigate(['../', 'read', id],
      { relativeTo: this.route }
    );
  }

  ngOnInit(): void {
    this.emailsAndTitle$ = this.route.paramMap.pipe(
      map(paramMap => paramMap.get('type')),
      map(type => {
        this.type = type;
        if (!type) {
          return {
            emails: (FAKE_EMAILS_DATA as Email[]).filter((data) => data.status === 'INBOX'),
            title: 'Boîte de réceptions'
          };
        }
        return {
          emails: (FAKE_EMAILS_DATA as Email[]).filter((data) => data.status === type?.toUpperCase()),
          title: type === 'sent' ? 'Emails envoyés' : 'Corbeille'
        };
      })
    )

    // this.route.paramMap.subscribe((paramsMap) => {
    //   const type = paramsMap.get('type');

    //   if (!type) {
    //     this.emails = (FAKE_EMAILS_DATA as Email[]).filter(
    //       (email) => email.status === 'INBOX'
    //     );
    //     return;
    //   }

    //   this.emails = (FAKE_EMAILS_DATA as Email[]).filter(
    //     (email) => email.status === type?.toUpperCase()
    //   );
    // });

  }

}
