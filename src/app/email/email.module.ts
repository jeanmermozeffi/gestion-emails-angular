import { NgModule } from "@angular/core";
import { ListeComponent } from "./liste/liste.component";
import { DetailsComponent } from "./details/details.component";
import { DashbordComponent } from "./dashbord/dashbord.component";
import { CreateComponent } from "./create/create.component";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: '',
    component: DashbordComponent,
    children: [
      { path: '', component: ListeComponent },
      { path: 'create', component: CreateComponent },
      { path: ':type', component: ListeComponent },
      { path: 'read/:id', component: DetailsComponent },
    ]
  },
]

@NgModule({
  declarations: [
    DashbordComponent,
    ListeComponent,
    DetailsComponent,
    CreateComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EmailModuele {

}
