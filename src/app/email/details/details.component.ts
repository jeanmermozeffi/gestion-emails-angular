import { Component, OnInit } from '@angular/core';
import { Email } from '../types';
import { ActivatedRoute, Router } from '@angular/router';
import { FAKE_EMAILS_DATA } from '../../data';
import { Observable, filter, map } from 'rxjs';

@Component({
  selector: 'app-email-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  email$?: Observable<Email>

  getBodyPart(body: string){
    return body.split('\n');
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  deleteMail() {
    return this.router.navigate(
      ['../../'],
      { relativeTo: this.route, }
    )
  }

  ngOnInit(): void {
    this.email$ = this.route.paramMap.pipe(
      filter(params => params.has('id')),
      map(params => {
        return +params.get('id')!
      }),
      map(id => {
        return FAKE_EMAILS_DATA.find(email => email.id === id) as Email;
      })
    )
    // this.router.paramMap.subscribe(params => {
    //   if (!params.has('id')) {
    //     return;
    //   }
    //   const id = +params.get('id')!;

    //   this.email = FAKE_EMAILS_DATA.find(email => email.id === id) as Email;
    // })

  }

}
